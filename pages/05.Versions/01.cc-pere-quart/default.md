---
title: '2017 - CC Pere Quart'
media_order: 'presentacio-collage.png,maquineta-collage-lcl.png,maquineta-decoracio-collage.png,snaparcade-introduccio-collage-lleuger.png'
published: true
---

La primera màquina va ser un encàrrec del Centre Cívic Pere Quart del barri de Les Corts de Barcelona. Es va proposar coordinar el disseny amb algun col·lectiu del barri per tal que dissenyés la carcassa en homenatge a **Joan Oliver / Pere Quart**. 

El literat sabadellenc Joan Oliver, de família benestant, signava els seus poemes amb el pseudònim Pere Quart, i d'aquí el nom dual. L'estil narratiu de l'autor, marcat per guerres i exili, destil·la un escepticisme i ironia que ridiculitzen la burgesia, la societat de consum i, en general, les condicions establertes. Oliver, juntament amb molts altres com Mercè Rodoreda, Carles Riba o Pere Calders, va haver de fugir i exiliar-se a causa de la repressió franquista. 
![](maquineta-decoracio-collage.png)
Per a la decoració de la maquineta, el Centre Cívic Pere Quart es va posar en contacte amb una escola de disseny però la proposta no acabava d'encaixar, així que des del centre van buscar a un artista a qui van encarregar el disseny de la carcassa. I es va inspirar del **Bestiari** de Pere IV, una obra de 1937. Una de les versions més conegudes d'aquest bestiari és la que fou il·lustrada per Xavier Nogués, artista barceloní que usava el pseudònim de Babel. 

Un Bestiari és una obra de tradició medieval que recull bèsties i monstres explicant la seva simbologia. Té una funció didàctica, ja que s'associen virtuts i defectes humanes a les bèsties, com es fa a les fabules. En paraules de Pere Quart: “_la natura, diligent, ens procura una bèstia per a cada molèstia_”. 
![](maquineta-collage-lcl.png)
La Snap!Arcade de Pere Quart es va pensar a l'estiu, es va construir a la tardor i es va presentar a l'hivern, exactament el **8 de gener de 2018**. De gener a març es va programar un curset per aprendre a programar videojocs amb l'objectiu de pujar a la màquina els jocs creats. Al final, però, no es van enviar jocs per pujar i van quedar els que, juntament amb el projecte de presentació de la màquina, hi havíem pujat: el [Cave](https://snap.berkeley.edu/project?user=bromagosa&project=cave), el Jumpy Kitten (en reparació), el [Tron](https://snap.berkeley.edu/project?user=bromagosa&project=Tron), el [Pong](https://snap.berkeley.edu/project?user=bromagosa&project=Pong). Aquí, un parell d'imatges de la presentació de la Snap!Arcade al Centre Cívic Pere Quart:
![](presentacio-collage.png)
El projecte de presentació és [mig historieta, mig videojoc](https://snap.berkeley.edu/project?user=gamificat&project=arcade-1-llantia-oliver). La història arrenca amb un ninot que representa a en Joan Oliver: troba i frega una llàntia, i se li apareix una geni per concedir-li un desig. Oliver demana una màquina arcade i hi va a espategar a dins. Un cop el literat és dins la màquina va a buscar a un col·lega seu (Pere Calders) i comença el joc. Plouen _marcianus_ (space invaders) que resten vides. El ninot de l'Oliver pot llençar barrets per eliminar _marcianitus_; i el de Calders, llibres. És un joc curt, per a dues jugadores, i guanya el ninot del literat que resisteix més estona la pluja de _marcianus_. 
![](snaparcade-introduccio-collage-lleuger.png)
Si juguem a l'ordinador i no a la Snap!Arcade, hem de jugar amb les lletres del teclat: l'Oliver es mou amb la tecla A i D, i dispara amb la R; i el Calders, es mou amb la J i la L, i dispara amb la Y. I hi ha una **drecera** per si es vol saltar la historieta de la llàntia: si es prem qualsevol tecla en qualsevol moment, anem directament al joc. 

Si trobeu interessant el projecte, us convidem a crear-vos la vostra, a contactar-nos o a fer-ne difusió a les xarxes socials amb l'etiqueta **#SnapArcade**.


