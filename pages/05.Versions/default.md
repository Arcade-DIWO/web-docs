---
title: Versions
media_order: 'IMG_20180115_114417-retall.jpg,snap-arcade-lleialtat-fons-retall.jpeg'
---

Al 2017 vam fer un primer disseny en 3D per al [Centre Cívic Pere Quart](https://snaparcade.cat/versions/cc-pere-quart). La decoració de la carcassa ret homenatge a l'escriptor català que dóna nom al centre. L'artista contractat es va inspirar del Bestiari de Pere Quart per a la seva decoració.
![](IMG_20180115_114417-retall.jpg)


Al 2019 vam fer un segon disseny 3D per a la de la [Lleialtat Santsenca](https://snaparcade.cat/versions/2019-la-lleialtat-santsenca). La temàtica de la decoració es va triar des de la comissió d'Acció Comunitària, i en ella, hi van participar la comissió de Memòria Històrica i el Col·lectiu d'Artistes de Sants. El tema triat fou la reivindicació popular: d'un cantó hi ha les tres xemeneies en honor a la Vaga de La Canadenca, que al febrer de 2019 celebrava els seus 100 anys; i de l'altre, es representen detalls d'una vaga al barri per reivindicar l'escola pública. Al frontal, hi ha l'antic logotip de la Lleialtat quan era cooperativa.
![](snap-arcade-lleialtat-fons-retall.jpeg)