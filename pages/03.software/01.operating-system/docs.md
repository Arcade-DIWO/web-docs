---
title: 'Operating System'
taxonomy:
    category:
        - docs
---

Traditionally arcade machines run games but we call them Projects, because software written in Snap! does not necesarily need to be a game.

Snap projects run inside a browser. We use Chromium for that.

Arcade machine users need a way to select and run the projects. We use another, lesser known browser, Midori to display the Main Menu.

The Main Menu works like this. Projects are listed on the left and are navigated Up and Down with either of the joysticks. Details of the highlighted project are displayed on the right. Any of the 'Fire' buttons start the highlighted project (in the Chromium browser). The center button stops (pkill) the project.

![](machine-menu.png)

This is achieved by running a lightweight http server locally on the machine. When the machine boots, the server is started. It servers index.html (the Main Menu) to the Midori browser and exposes two endpoints, http://localhost/listProjects and http://localhost/start, that are called via javascript in index.html


### Installation
The Arcade machine runs Debian version 9 operating system. [Download it here](https://www.debian.org/). Burn the ISO to a USB device and install.

During the installation create a user called **arcade** The path /home/arcade is hard-wired into the software.

![](createuser.jpg)


#### Software selection

When selecting pakages, install "Standard system" only.

!! Do not install a Desktop environment such as Gnome, KDE, or any other.

![](debianinst25.png)

After the Debian installation has finished, reboot and install these packages. 

    apt-get update && apt-get -y upgrade 
    apt-get -y install sudo ssh xinit ratpoison lxterminal git curl python-pip python-virtualenv midori chromium vim
    apt-get clean

Gunicorn listens on ip4. Unfortunatley Midori calls locahost on ip6. No problem, let's disable ip6

    echo net.ipv6.conf.all.disable_ipv6=1 > /etc/sysctl.d/disableipv6.conf


### Install the HTTP server

We use a simple HTTP server written in [Flask](http://flask.pocoo.org/) to create an interface that lists, selects, and runs Snap! projects.

Login as the user 'arcade'

    cd /home/arcade
    virtualenv venv
    source /home/arcade/venv/bin/activate
    pip install flask lxml gunicorn

Clone the software

    cd /home/arcade
    git clone https://gitlab.com/Arcade-DIWO/project-manager.git arcade
    git clone https://gitlab.com/Arcade-DIWO/common-projects.git arcade/projects/common
    mkdir /home/arcade/arcade/projects/local
    ln -s /home/arcade/arcade/projects/ /home/arcade/arcade/server/static/

    mkdir /home/arcade/.fonts
    git clone https://github.com/jmoenig/Snap--Build-Your-Own-Blocks arcade/snap
    ln -s /home/arcade/arcade/snap/ /home/arcade/arcade/server/static/
    cp /home/arcade/arcade/server/static/raleway/Raleway-Light.ttf /home/arcade/.fonts/
    cp /home/arcade/arcade/server/config.cfg.example /home/arcade/arcade/server/config.cfg

!!! Edit /home/arcade/arcade/server/config.cfg

### Autostart gunicorn

```
apt-get install supervisord
```

edit `/etc/supervisor/conf.d/gunicorn.conf`

```
[program:gunicorn]
command = /home/arcade/venv/bin/gunicorn -c /home/arcade/arcade/gunicorn.py server:app
directory = /home/arcade/arcade
user = arcade
```

edit `/home/arcade/arcade/gunicorn.py`
```
command = '/home/arcade/venv/bin/gunicorn'
pythonpath = '/home/arcade/arcade'
bind = '0.0.0.0:8000'
workers = 5
user = 'arcade'
```

### Configure ratpoison

Edit `/home/arcade/.ratpoisonrc`

```
startup_message off

# Add key bindings:
definekey top b exec ~/arcade/bash/kill_chromium.sh &
definekey top t exec /usr/bin/amixer -q sset Master 1%+
definekey top g exec /usr/bin/amixer -q sset Master 1%-

# What programs should be run on start up?
exec ~/arcade/bash/start_midori.sh

```
    

### Autologin and hide mouse

Login as root and install and configure.

    apt-get install lightdm
    
Edit /etc/lightdm/lightdm.conf

    [Seat:*]
    xserver-command =X -nocursor
    autologin-user= arcade

## DEFUNCT
We have changed the window manager from openbox to ratpoison. These are the old openbox notes

### Configure openbox

    mkdir -p /home/arcade/.config/openbox
    cp -r /etc/xdg/openbox/* /home/arcade/.config/openbox/

Edit /home/arcade/.config/openbox/rc.xml and add a <keybind> to the <keyboard> section
    
    <openbox_config xmlns="http://openbox.org/3.4/rc" xmlns:xi="http://www.w3.org/2001/XInclude">
       <keyboard>
          .....
          <keybind key="b">
          <action name="Execute"><command>~/arcade/bash/kill_chromium.sh</command></action>
          </keybind>
          <!-- Volume control. Joystick + button combination generate these keystrokes -->
  		  <keybind key="t">
          <action name="Execute"><command>amixer -q sset Master 3%+</command></action>
  	      </keybind>
  		  <keybind key="g">
          <action name="Execute"><command>amixer -q sset Master 3%-</command></action>
  		  </keybind>
          .....
       </keyboard>
    </openbox_config>

Now the Key 'b' will kill the Chromium browser running the Snap! projects. This efectively takes the user back to the Main menu.

### Autostart midori
Edit /home/arcade/.config/openbox/autostart and add..

    # sleep here to give gunicorn a chance to start.
    sleep 2
    /usr/bin/midori -e Fullscreen -a http://localhost:8000 &
    #~/arcade/bash/start-midori


## DEFUNCT!!
This can lead to corrupted repos if the machine reboots while pulling. :(

### Update the server software and snap project repos

30 seconds after booting (when we have a network), we run a script to update server software and Snap! projects.

Edit /etc/systemd/system/arcade-update.service

    [Unit]
    Description=Update Arcade
    Wants=network-online.target
    After=network.target network-online.target
    
    [Service]
    ExecStart=/home/arcade/arcade/bash/update.sh
    
    [Install]
    WantedBy=multi-user.target

Save that file and now enable it

    systemctl enable arcade-update.service
