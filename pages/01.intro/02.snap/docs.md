---
title: Snap!
media_order: logo-snap-alonzo.png
taxonomy:
    category:
        - docs
---

La màquina funciona amb [**Snap!**](http://snap.berkeley.edu/) que és al mateix temps una comunitat i  un senzill i potent **llenguatge de programació visual** que permet crear i compartir continguts digitals com videojocs, historietes animades, presentacions interactives, il·lusions òptiques i tot allò que siguem capaços d'imaginar! 
![](logo-snap-alonzo.png)
Snap! és el germà gran de [**Scratch**](https://scratch.mit.edu/) i conté nombroses funcionalitats que permeten programar projectes molt més complexos. A més, gràcies a la **llibertat i plasticitat** de la seva arquitectura, altres desenvolupadors/es se serveixen del codi font de Snap! per adaptar-lo a les seves necessitats. 

En són exemple [**Beetle Blocks**](http://beetleblocks.com/), per al disseny 3D i representació de dades; [**Snap4Arduino**](http://snap4arduino.rocks/) o [**MicroBlocks**](http://microblocks.fun/), per a la programació d'enginys electrònics; i [**Turtle Stitch**](http://turtlestitch.org/), una modificació de Snap! que permet brodar allò que programem.

Pots trobar recursos per aprendre a programar amb Snap! en català a [Gamifi.cat](http://www.gamifi.cat/tag/snap/) i a la Xarxa Telemàtica d'Ensenyament de Catalunya, en dos magnífics cursos: [curs 1](http://ateneu.xtec.cat/wikiform/wikiexport/materials/scl/scl1/index) i [curs 2](http://ateneu.xtec.cat/wikiform/wikiexport/materials/scl/scl2/index). En anglès, hi ha el curs ["The Beauty and Joy of Computing"](https://bjc.edc.org/bjc-r/course/bjc4nyc.html), conegut com a BJC, de la universitat de Berkeley, Califòrnia. 

Si t'entrebanques amb algun projecte, pregunta a la comunitat. El [fòrum de Snap!](https://forum.snap.berkeley.edu/) és un espai on resoldre dubtes, fer propostes per a noves implementacions, etc. L'usuari i la contrasenya per al fòrum seran els mateixos que fas servir per a crear els projectes al núvol de Snap!.

##### Snap!

_The machine runs projects programmed in [**Snap!**](http://snap.berkeley.edu/) a simple but potent **visual programming language** that let's us create and share digital content like video games, story-telling, interactive presentations, optical ilusions and everything else you could possibly imagine!_

_Snap! is the big sister of [**Scratch**](https://scratch.mit.edu/) and contains numerous functionalities the let us programme projects much more complex. Moreover, thanks to the **freedom and plasticity** of it's architecture, other developers take the source code of Snap! and adapt it to their needs._

_Examples of that are [**Beetle Blocks**](http://beetleblocks.com/), for 3D design and data representation; [**Snap4Arduino**](http://snap4arduino.rocks/) or o [**MicroBlocks**](http://microblocks.fun/), for programming electronic gadgets; and [**Turtle Stitch**](http://turtlestitch.org/), a modification of Snap! that sews on our command._

_You can find resources to learn how to program with Snap! in catalan on [Gamifi.cat](http://www.gamifi.cat/tag/snap/) and two wonderful courses from la Xarxa Telemàtica d'Ensenyament de Catalunya: [course 1](http://ateneu.xtec.cat/wikiform/wikiexport/materials/scl/scl1/index) i [course 2](http://ateneu.xtec.cat/wikiform/wikiexport/materials/scl/scl2/index). In english, there's  a course ["The Beauty and Joy of Computing"](https://bjc.edc.org/bjc-r/course/bjc4nyc.html), known as BJC, made by University of California, Berkeley and Education Development Center._

_If you get some trouble with a project ask the community. [Snap! Forum](https://forum.snap.berkeley.edu/) is an space to solve questions, ask for features, etc. The user and password for the forum are the same that you use to create projects on the Snap! Cloud._