---
title: Projectes
taxonomy:
    category:
        - docs
---

Amb Snap! podem programar tot tipus de projectes, no només videojocs! Podem crear **acudits, historietes, efectes òptics, simulacions, generadors d'avatars, presentacions, projectes d'art i música**, i tot allò que imaginem. Aquí alguns projectes:

* [Acudit](https://snap.berkeley.edu/project?user=gamificat&project=acudit-gif-png) 
* [Acudit interactiu](https://snap.berkeley.edu/project?user=gamificat&project=Se-Abre-El-Telon-Paca)
* [Historieta](https://snap.berkeley.edu/project?user=gamificat&project=un-altre-sant-jordi)
* [Efecte òptic](https://snap.berkeley.edu/project?user=uoc_tpi&project=BridgetRiley_)
* [Simulació](https://snap.berkeley.edu/snapsource/dev/snap.html#present:Username=jens&ProjectName=Earth%20Venus%20Orbits)
* [Generador d'avatars](https://snap.berkeley.edu/project?user=gamificat&project=avatar-pop)
* [Presentació](https://snap.berkeley.edu/project?user=bromagosa&project=Snap!%20Arcade%20Machine)
* [Projecte artístic](https://snap.berkeley.edu/project?user=youngthinkers&project=Remixing%20Mondrian) 
* [Projecte musical](https://snap.berkeley.edu/snapsource/snap.html#present:Username=jens&ProjectName=Frere%20Jacques&editMode)

A la màquina Snap!Arcade també hi podem posar projectes per a gaudir-ne **dues personetes**, com aquests:

* [Pong](https://snap.berkeley.edu/project?user=bromagosa&project=Pong)
* [Soccer](https://snap.berkeley.edu/project?user=milliones&project=Supplementary%209%3A%20Snap%20Soccer)
* [Wizard](https://snap.berkeley.edu/project?user=petermyip&project=wizwar)
* [Tron](https://snap.berkeley.edu/project?user=bromagosa&project=Tron)

[Explora](https://snap.berkeley.edu/explore) tots els projectes de Snap! a la seva comunitat. Allà podràs crear un compte per desar els teus projectes al núvol de Snap!, que així com el seu llenguatge té **llicència lliure AGPL**. Si vols instal·lar Snap! en local per no dependre d'Internet per programar els teus projectes, [descarrega el programa](https://github.com/jmoenig/Snap) al teu ordinador des del compte d'en Jens, un dels seus creadors. No cal instal·lar res, només cal descomprimir l'arxiu descarregat. Un cop a dins la carpeta descomprimida, busca l'arxiu anomenat **"index.html"** i fes-hi doble clic al damunt: Snap! s'executarà al teu navegador. Pim pam, així de fàcil! 

Si vols exemples de projectes ben diversos, fes una ullada al [document d'exemples](https://snap.berkeley.edu/SnapExamples.pdf). El document és en anglès però un cop a dins dels projectes d'exemple podem **canviar l'idioma** per entendre com funcionen. El llenguatge Snap! està traduït a una pila d'idiomes, entre ells, el català, així que a diferència d'altres llenguatges no-visuals com Python o Java, els llenguatges de programació visual com Snap! fomenten les llengües minoritàries. A més, no cal compilar-los perquè s'executen **en temps real**; i no tenen sintaxi, amb el que l'aprenentatge del llenguatge no es dificulta per foteses com punts i coma, com passa amb la sintaxi dels llenguatges visuals. Crea els teus projectes amb [Snap!](https://snap.berkeley.edu/)
