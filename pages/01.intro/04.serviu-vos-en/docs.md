---
title: Serviu-vos-en!
taxonomy:
    category:
        - docs
---

En aquesta secció, us convidem a **replicar el projecte** de la màquina Snap!Arcade al vostre entorn. A més de la producció física de la màquina, us animem a col·laborar amb nosaltres creant jocs i posant les màquines en xarxa. 

Actualment, la màquina conté un **repositori local** de jocs, on s'emmagatzemen alguns jocs que hem programat amb Snap!. Però també hi ha creat un **repositori comú**, on poder compartir jocs amb altres possibles màquines. Qui es vol enxarxar? ;) 

A la part 2 i 3 trobareu la recepta de la màquina! Serviu-vos-en i feu-vos-la vostra! A l'apartat de [hardware](https://snaparcade.cat/hardware), trobareu com muntar tota la **part física**. A l'apartat de [software](https://snaparcade.cat/software) trobareu com instal·lar i executar el **programari** necessari. 

Si us fa il·lusió tenir una Snap! Arcade enxarxada però no us veieu en cor de fer-ho vosaltres mateixes, contacteu-nos a [info@snaparcade.cat](mailto:info@snaparcade.cat).

##### Serve yourself!

_We would like to invite you to **replicate the Snap! machine project** in your neighbourhood. Not only the machine itself, but we also encourage you to collaborate with us programming games to upload to the machines._

_The machine is designed to load games that we programme in Snap! from two different git repositories. The 'local' repo is for games that will only be loaded into your machine, and the 'common' repo is for games that are shared between all machines. Who's ready to get [networked](https://snaparcade.cat/networked)?_

_In the following pages of this website you can find the machine's construction recipe! Serve yourself and make it you own! In [hardware](https://snaparcade.cat/hardware), you'll find instructions about the **phisical part**. In [software](https://snaparcade.cat/software) we've documented the **software installation** of the machine itself._

_If you'd like to have your own networked Snap! Arcade but feel you can't do it yourself, get in touch with us here: [info@snaparcade.cat](mailto:info@snaparcade.cat)._