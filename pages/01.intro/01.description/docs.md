---
title: Descripció
taxonomy:
    category:
        - docs
---

**Un món per entendre i recrear**

Vivim en un món cada vegada més digital i els canvis se succeeixen tan ràpid que no tenim gairebé temps per aturar-nos a reflexionar sobre totes aquestes **màquines i programes** que ens envolten. 	

La creació d'aquesta màquina recreativa, a més de fomentar la col·laboració entre diversos agents, pretén ser un aparador que desperti l'interès de la ciutadania en relació a les **tecnologies lliures, ètiques i distribuïdes**. 

Si bé l'objectiu de la primera fase del projecte és la pròpia construcció de la màquina, la segona fase té un doble objectiu:

* **Crear contingut digital** per la màquina amb eines lliures, fomentant així l'aprenentatge de nous conceptes relacionats amb les tecnologies creatives i el pensament computacional.
* **Apropar conceptes i eines** relacionades amb la sobirania tecnològica per tal d'apoderar el veïnat en la reflexió i l'us d'infraestructura i programari lliures, en la comprensió d'una realitat canviant on afloren noves competències i oficis

##### Description

_We live in a world becoming more and more digital where changes are happening so fast that we hardly have time to stop and think about all the **harware and software** that surrounds us._

_The contruction of this machine, a part from promoting collaboration between different people involved, aims to be a 'shop window' that provokes interest in **free, ethical and distributed techonolgies** within the citizenship._

_Whereas the objective of the first phase of this project is to buid the arcade machine, the second phase has a double objective:_

* _**Create digital content** for the machine using free tools to forment the learning of new concepts realted to creative techologies and computer science._
* _Make common **concepts and tools** related with techological soverienty to help empower the neighbourhood so as to be able to make a personal reflection about free software and infraestructure._