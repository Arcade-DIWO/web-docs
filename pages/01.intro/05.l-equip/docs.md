---
title: 'L''equip'
media_order: equip-17-19.png
taxonomy:
    category:
        - docs
---

L'equip de la Snap!Arcade el formem el Bernat, el Chris i la Rita, tres entusiastes del programari lliure i el llenguatge de programació Snap!

Estem disponibles per a construïr màquines Snap!Arcade en diferents formats, des de la construcció des de zero pel nostre compte fins a un format més col·laboratiu on la teva comunitat participi activament del procés.

Si vols contactar amb nosaltres, pots fer-ho mitjançant el correu info@snaparcade.cat
![](equip-17-19.png)

##### The team

The Snap!Arcade team is made up of three people, free software and Snap! programming language enthusiasts. Bernat, Chris & Rita.

We are here to help you have a machine on your premises. We can build on for you, or we can help you build it collaboratively so your community can participate in the process.

If you want to contact us, we can be found here. info@snaparcade.cat

