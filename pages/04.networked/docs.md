---
title: Networked
taxonomy:
    category:
        - docs
---

Software is built to be used, shared, and studied.

The Snap! Arcade machine operating system is designed to share projects programmed in Snap! between the different machines when connected to the Internet.

When the machine boots, two directories are scanned for projects and then displayed on the Main menu. Each of these directories are in fact clones of git repositories that are configured during the [installation process](https://snaparcade.cat/software/operating-system).

##### The Local repository

Projects contained within have been developed locally by the people and students developìng software at the Civic Centre in question.

The repository is managed by tutors or staff at the civic centre. They can upload new projects created by students so everyone in the neighbourhood who passes by can have a look, run the projects, and enjoy locally developed software. We can expect some of these projects to contain content and themes relevant to the locality. This repository is not shared between machines.

On the other hand, other projects may be of general interest reaching further than a strictly local environment. These project can be included in the Common repository.


##### The Common repository

Projects that might be enjoyed by a wider audience are uploaded to this [git repository](https://gitlab.com/Arcade-DIWO/common-projects).
When the machine boots it checks for an Internet connection, and if it is online then syncs the repositories, downloading projects onto the machine.
The Common repository is managed by the [Snap! arcade team](https://snaparcade.cat/intro/l-equip).

By this means we share projects between machines, enriching the repertoire of projects, giving visibility to software developers, and creating community.

