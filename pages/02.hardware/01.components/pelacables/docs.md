---
title: Pelacables
taxonomy:
    category: docs
process:
        twig: true
---

### Pelacables

No és imprescindible, però un pelacables ens anirà bé per exposar el filament de coure dels cables abans de soldar-los.

![](pelacables.jpg)
*Foto: [Pixabay](https://pixabay.com/en/pliers-tool-wire-stripper-craft-1031979/) (CC0)*

Si hi teniu pràctica, podeu estalviar-vos aquesta eina i pelar els cables amb unes [alicates de tall](../alicates).
