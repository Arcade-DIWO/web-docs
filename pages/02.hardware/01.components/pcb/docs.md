---
title: Placa de prototipatge (perfboard)
taxonomy:
    category: docs
process:
        twig: true
---

### Placa de prototipatge (perfboard)

Tot i que no és imprescindible, ens anirà bé poder agrupar totes les connexions a 5v i a neutre en una placa de prototipatge, o *perfboard*.

En podem trobar a qualsevol botiga de components electrònics, i la més petita que trobem ens servirà.

![](perfboard.png)
*Foto: [Klaus - Günter Leiss - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:StripBoard.png) (GFDL)*
