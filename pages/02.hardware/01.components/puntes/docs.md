---
title: Puntes de cargolar
taxonomy:
    category: docs
process:
        twig: true
---

### Puntes de cargolar

Si utilitzeu una [cargoladora](../cargoladora) (molt recomanable), necessitareu puntes per als tipus de [cargols](../cargols) particulars que hàgiu adquirit.

![](puntes.jpg)

*Foto: [Afrank99](https://commons.wikimedia.org/wiki/User:Afrank99) [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:TorqSet-Bits.jpg) (CC BY-SA 3.0)*
