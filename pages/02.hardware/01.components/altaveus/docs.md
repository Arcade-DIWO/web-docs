---
title: Amplificador i altaveus
taxonomy:
    category: docs
process:
        twig: true
---

### Amplificador i altaveus

La manera més senzilla i econòmica d'aconseguir uns altaveus i un amplificador és comprar uns altaveus per a PC i desmuntar-ne la carcassa. En el cas de la màquina del CC Pere Quart, vam optar per adquirir-ne uns de senzills a un basar.

![](altaveus.jpg)

*Foto: [Pxhere](https://pxhere.com/es/photo/1387049) (CC0)*

Si el projecte requereix una roda de control de volum extern, probablement us caldrà dessoldar el potenciòmetre integrat al circuit de l'amplificador i soldar-ne un de nou amb la llargada de cable necessària. Consulteu l'apartat d'[electrònica](../electronica) per a més informació sobre aquest circuit.
