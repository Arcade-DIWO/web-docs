---
title: Massilla de fusta
taxonomy:
    category: docs
process:
        twig: true
---

### Massilla de fusta

Per a omplir algunes possibles ranures i també els possibles forats de subjecció de les planxes de contraplacat al llit de la fresadora CNC podeu utilitzar massilla per a fusta. Apliqueu-la amb una [paleta](../paleta) fins deixar la superfície ben plana, deixeu-la assecar, poliu-la suaument amb paper de vidre, i repetiu el procés fins que quedi ben a ras de la superfície que esteu reparant.
