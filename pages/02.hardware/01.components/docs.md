---
title: Components
taxonomy:
    category: docs
process:
	twig: true
---

### Llistat de components

Llistem a continuació tots els components necessaris per a la construcció de la màquina i, en la majoria dels casos, on podeu obtenir-los.

#### Components comercialment disponibles

* [Ordinador de sobretaula](pc).
* [Joysticks](joystick).
* [Botons](botons).
* [Arduino Leonardo](arduino).
* [Cable microUSB](microusb).
* [Amplificador i altaveus](altaveus).
* [Cable minijack 3.5mm](minijack).
* [Potenciòmetre per al control de volum](potenciometre).
* [Connector d'alimentació](socket).
* [Ventiladors](ventiladors).
* [Pantalla 4:3 de 19"](pantalla).
* [Panys](panys).
* [Resistències de 220Ω](resistencies).
* [Allargador Ethernet](ethernet).
* [Placa de prototipatge](pcb).
* [Lladre](lladre).
* [Rodes](rodes).
* [Frontisses de piano](frontisses).

#### Components que requereixen fabricació digital
* [Xassís](../02.chasis).
* [Suport per als connectors](suport).

#### Material fungible
* [Cargols](cargols).
* [Cable de 9v](cable-9v).
* [Estany de soldar](estany).
* [Plàstic termofusible](plastic).
* [Brides](brides).
* [Cinta per a juntes de finestra](cinta-juntes).
* [Cinta de doble cara](cinta-doble).
* [Massilla de fusta](massilla).
* [Super glue](superglue).
* [Cola blanca](cola).

#### Eines

* [Pistola de plàstic termofusible](pistola).
* [Trepant](trepant).
* [Broques](broques).
* [Cargoladora](cargoladora).
* [Puntes de cargolar](puntes).
* [Soldador](soldador).
* [Massa de fusta](massa).
* [Llimes i paper de vidre](llimes).
* [Alicates](alicates).
* [Pelacables](pelacables).
* [Paleta](paleta).

#### Maquinària

* [Fresadora CNC](cnc).
* [Impressora 3D](3d).
