---
title: Pantalla
taxonomy:
    category: docs
process:
        twig: true
---

### Pantalla

La carcassa està dissenyada per a acceptar una pantalla de **19 polzades** amb **relació d'aspecte 4:3** (no panoràmica). Actualment, costa trobar pantalles noves que no siguin panoràmiques, però en podeu aconseguir de molt barates i en molt bon estat a botigues de segona mà.

Fixeu-vos sobretot que s'encengui automàticament en rebre senyal, ja que no quedarà cap botó de la pantalla exposat a l'exterior.

No us preocupeu per les mides exactes de la pantalla. El disseny està pensat per acomodar qualsevol possible geometria.

Compteu que la pantalla necessitarà també el seu cable de corrent i un cable de vídeo (habitualment VGA o HDMI).
