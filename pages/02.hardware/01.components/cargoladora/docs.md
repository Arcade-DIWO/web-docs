---
title: Cargoladora
taxonomy:
    category: docs
process:
        twig: true
---

### Cargoladora

Tot i que no és totalment imprescindible i pot ser substituïda per un tornavís manual, una cargoladora reduirà molt substancialment el temps de muntatge de la la màquina. Si no en teniu, un trepant amb velocitat regulable també us servirà.

![](cargoladora.jpg)

*Foto: [Public Domain Pictures](http://www.publicdomainpictures.net/view-image.php?image=18137) (CC0)*
