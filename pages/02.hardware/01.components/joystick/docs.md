---
title: Joystick
taxonomy:
    category: docs
process:
        twig: false
---

### Joystick

Us faran falta dos joysticks digitals, un per a cada jugador. Recomanem no estalviar en qualitat per als components que han de ser utilitzats pel públic i, per tant, rebran més desgast.

Nosaltres hem utilitzat un model marca CEBEK que podeu trobar a Onda Radio:

* [Joystick Digital CEBEK](http://www.ondaradio.es/producto/c5295-cebek-joystick-digital-5555.aspx)
