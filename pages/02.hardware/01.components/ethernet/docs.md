---
title: Allargador Ethernet
taxonomy:
    category: docs
process:
        twig: true
---

### Allargador Ethernet

Per poder connectar la màquina a la xarxa, cal que la roseta d'Ethernet sigui accessible des de l'exterior. Per a això, caldrà un allargador de cable de xarxa que puguem connectar a la tarja de xarxa de l'ordinador i acoblar a la part posterior de la carcassa, preferiblement utilitzant un [suport imprès en 3D](../suport).

No és especialment fàcil trobar-ne a botigues, així que nosaltres l'hem obtingut online:

* [Allargador Ethernet](https://www.amazon.es/gp/product/B00QV0K2QA)
