---
title: Rodes
taxonomy:
    category: docs
process:
        twig: true
---

### Rodes

Un cop muntada, la màquina és força feixuga, de manera que no és gens mala idea incorporar-hi unes rodes. Cal que estiguin preparades per a suportar el pes de la màquina (1/4 part del pes per roda), i és preferible que les dues rodes de davant es puguin fixar per evitar que la màquina es desplaci mentre hi juguem.

A qualsevol ferreteria us podran aconsellar sobre les rodes que necessiteu.

![](rodes.jpg)
*Foto: [Richard Binstadt - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Generic_Caster.jpg) (CC BY-SA 4.0)*
