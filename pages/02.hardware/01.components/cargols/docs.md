---
title: Cargols
taxonomy:
    category: docs
process:
        twig: true
---

### Cargols

Necessitareu força cargols per a fusta de mides diferents. Compteu amb un mínim de:

* 50 cargols de 12x3mm
* 50 cargols de 22x3mm
* 15 cargols de 38x3mm

Les llargades poden variar, però cal tenir en compte que:
* Els de **12mm** us caldran per a unir components a la carcassa, com ara frontisses, altaveus o botons. El gruix de la fusta és de 15mm i no volem arribar a travessar-la.
* Els de **22mm** són estructurals i serveixen per ensamblar la carcassa en sí. Aquests hauran de travessar una primera planxa de contraplacat i entrar suficientment a la segona.
* Els de **38mm** serveixen per unir els suports de la pantalla a la carcassa, i han de travessar dues planxes de contraplacat i entrar suficientment en una tercera.
