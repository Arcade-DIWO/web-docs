---
title: Fresadora CNC
taxonomy:
    category: docs
process:
        twig: true
---

### Fresadora CNC

Per al tall de les fustes de la carcassa necessitareu accés a una fresadora CNC de grans dimensions. En podeu trobar a molts FabLabs, ateneus de fabricació o makerspaces.

Nosaltres la vam dur a tallar al [Fab Lab Barcelona](https://fablabbcn.org/), situat al barri del Poble Nou de Barcelona. Al Fab Lab hi tenen dues fresadores [ShopBot](https://wiki.fablabbcn.org/ShopBot) de gran format on només haureu de portar el [gcode](../../chasis/gcode) que ja està preparat per a aquestes màquines.

Us caldran tres planxes de contraplacat de 225x150x15, que us poden proporcionar al mateix FabLab.
