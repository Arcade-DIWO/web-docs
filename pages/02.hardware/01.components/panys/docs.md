---
title: Panys
taxonomy:
    category: docs
process:
        twig: true
---

### Panys

Per garantir la seguretat dels components interns, és preferible que la màquina quedi tancada amb un pany. Per a la porta de davant n'hi ha prou amb un baldó que es pugui obrir i tancar des de dins de la màquina, mentre que a la part de darrera hi podem posar una tanca amb cadenat.
