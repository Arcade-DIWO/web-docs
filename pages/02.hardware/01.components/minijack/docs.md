---
title: Cable minijack de 3.5mm
taxonomy:
    category: docs
process:
        twig: true
---

### Cable minijack de 3.5mm

Necessitareu un cable d'àudio suficientment llarg com per connectar els altaveus a l'ordinador, a no ser que el cable integrat dels altaveus ja hi arribi. En el nostre cas, vam necessitar un cable mascle-femella de mig metre.
