---
title: Cable de 9v
taxonomy:
    category: docs
process:
        twig: true
---

### Cable de 9v

Per al muntatge de la botonera i els ventiladors necessitarem cable d'electrònica. Per fer-ho més entenedor, seria ideal comptar amb tres colors:

* **Vermell** per a les connexions a 5v
* **Negre** per a les connexions a neutre
* **Un tercer color** per al senyal electrònic cap a la placa controladora

Podeu aconseguir cable de 9v a qualsevol botiga d'electrònica, i no hauríeu de necessitar més de 5m de cada color.
