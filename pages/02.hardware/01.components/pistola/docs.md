---
title: Pistola de plàstic termofusible
taxonomy:
    category: docs
process:
        twig: true
---

### Pistola de plàstic termofusible

Té moltes aplicacions i no fa mai nosa. L'utilitzareu per enganxar aquells components que no es puguin perforar fàcilment o que vulguem adherir permanentment. També pot servir per aïllar algunes connexions elèctriques i per fixar cables a la carcassa.

![](pistola.png)

*Foto: [Pixabay](https://pixabay.com/en/glue-gun-glue-hot-gun-home-tool-1562969/) (CC0)*
