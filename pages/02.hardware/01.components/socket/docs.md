---
title: Connector d'alimentació
taxonomy:
    category: docs
process:
        twig: true
---

### Connector d'alimentació

Per tal que només calgui un cable per alimentar tots els components interns de la màquina (pantalla, ordinador i, possiblement, altaveus) utilitzarem un sòcol IEC_60320 C14 connectat a un [lladre de corrent](../lladre) modificat.

En podeu aconseguir un a qualsevol botiga de components electrònics.

![](socket.png)
![](socket-connexio.png)

*Fotos: pròpies (CC0)*
