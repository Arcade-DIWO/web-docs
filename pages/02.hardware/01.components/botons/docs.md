---
title: Botons
taxonomy:
    category: docs
process:
        twig: true
---

### Botons

En una màquina que ha d'estar de cara al públic és molt important que els botons siguin resistents i d'alta qualitat. Els botons que tradicionalment s'utilitzen per a les màquines *arcade* es coneixen com a *anti-vandàlics*.

![](botons.jpg)

*Foto: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Arcade_video_game_buttons.jpg) (Public Domain)*

Per a la màquina de Pere Quart, vam adquirir els botons a [Arcade Spare Parts](https://www.arcadespareparts.com), un distribuïdor online de components per a màquines *arcade*:

* [Standard Arcade Push Button](https://www.arcadespareparts.com/arcade_parts/push_buttons/arcade_button_standard_push_button_black/12131.html).

En necessitareu un total de 5: dos per a cada jugador i un de central per tornar al menú principal.

Si la vostra màquina ha d'estar en un ambient controlat on sabeu que els botons no patiran tant de desgast, podeu optar per opcions més econòmiques o, inclús, fabricar-los vosaltres mateixos mitjançant una impressora 3D i microinterruptors.
