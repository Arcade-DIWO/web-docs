---
title: Paleta
taxonomy:
    category: docs
process:
        twig: true
---

### Paleta

Per aplicar la massilla de fusta us farà falta una paleta o algun tipus d'espàtula plana.

![](paleta.png)

*Il·lustració: [Pixabay](https://pixabay.com/en/construction-site-spatula-spoonbill-1296141/) (CC0)*
