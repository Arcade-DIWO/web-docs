---
title: Cinta per a juntes de finestra (rivet)
taxonomy:
    category: docs
process:
        twig: true
---

### Cinta per a juntes de finestra (rivet)

Les portes de davant i darrera tenen una tolerància de 5mm, que és el diàmetre de la fresa de tall. Per salvar aquesta tolerància utilitzarem rivet, que és una cinta autoadhesiva que s'utilitza per a aïllar portes i finestres. En podreu trobar a qualsevol drogueria o centre de bricolatge. Us en caldran uns 6 metres.

![](rivet.png)

*Foto: pròpia (CC0)*

Si l'adhesiu de la cinta no és suficientment resistent, podeu utilitzar *super glue* per reforçar-lo.
