---
title: Suport per als connectors
taxonomy:
    category: docs
process:
        twig: true
---

### Suport per als connectors

El [connector d'alimentació](../socket) i el de [xarxa Ethernet](../ethernet) necessiten algun tipus de suport per tal que els puguem collar a la part de darrera de la màquina. Oferim un disseny en 3D que podeu imprimir directament amb una [impressora 3D](../3d), però si no teniu accés a una impressora sempre us el podeu fabricar a mà amb fusta o metacrilat i una serra de marqueteria.

![](suport.png)
![](suport-2.png)
![](suport-render.png)

*Fotos: pròpies (CC0)*  
*Render: [TinkerCAD](https://www.tinkercad.com/things/bAeNsuQq5H9-ethernet-socket-holder) (CC BY-SA 3.0)*

* [Fitxer STL](suport.stl) (CC BY-SA 3.0)
* [Enllaç TinkerCAD](https://www.tinkercad.com/things/bAeNsuQq5H9-ethernet-socket-holder) (CC BY-SA 3.0)
