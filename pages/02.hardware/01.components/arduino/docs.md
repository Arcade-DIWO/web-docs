---
title: Placa Arduino
taxonomy:
    category: docs
process:
        twig: true
---

### Placa Arduino

Els controls de joc simulen pulsacions de tecles enviant els mateixos senyals que les tecles d'un teclat d'ordinador qualsevol. Quan, per exemple, el joystick esquerre es mou cap amunt, un microinterruptor envia un senyal a una placa de control que, al seu torn, genera un senyal que simula la pulsació de la tecla **W** i l'envia a l'ordinador.

Per aconseguir aquest comportament, necessitarem una placa de prototipatge electrònic que pugui actuar com a interfície HID (Human Interface Device). [Arduino](http://arduino.cc) té algunes plaques al mercat que compten amb aquesta capacitat:

* [Arduino Leonardo](https://store.arduino.cc/arduino-leonardo-with-headers)
* [Arduino Micro](https://store.arduino.cc/arduino-micro)
* [Arduino Micro sense headers](https://store.arduino.cc/arduino-micro-without-headers) **recomanada**
* [Arduino Due](https://store.arduino.cc/arduino-due)

Tingueu en compte que també hi ha plaques compatibles amb Arduino que suporten aquesta funcionalitat, per exemple:

* [SparkFun Pro Micro](https://www.sparkfun.com/products/12640)

Tot i que nosaltres no ho hem testejat, si preferiu un muntatge més senzill podeu provar d'utilitzar una placa [Makey Makey](https://makeymakey.com/), que consta d'un circuit que simula un teclat i no requereix ser programada. El circuit electrònic també es simplificaria força. D'altra banda, el preu és força més elevat.

Per obtenir informació sobre el circuit, consulteu l'apartat d'[electrònica](../electronica).

Per obtenir informació sobre el codi, consulteu l'apartat de [software](../software/controles/).
