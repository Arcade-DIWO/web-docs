---
title: Soldador
taxonomy:
    category: docs
process:
        twig: true
---

### Soldador

Necessitareu un soldador elèctric per a realitzar les connexions de la botonera, els ventiladors i tot el cablejat elèctric.

![](soldador.jpg)
*Foto: [Александр Александрович Москвин - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Electri%D1%81_soldering_iron.jpg) (CC SA-BY 4.0)*
