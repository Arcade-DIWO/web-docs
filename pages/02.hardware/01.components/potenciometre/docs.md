---
title: Potenciòmetre
taxonomy:
    category: docs
process:
        twig: true
---

### Potenciòmetre

Si la vostra màquina necessita un control de volum extern, és possible que necessiteu un potenciòmetre.

Adquiriu primer els [altaveus](../altaveus) i comproveu si podeu reutilitzar el seu control de volum integrat. En cas que el control de volum estigui soldat al circuit integrat, serà necessari dessoldar-lo i substituir-lo per un potenciòmetre.

![](potenciometre.jpg)

*Foto: [Iain Fergusson - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Potentiometer.jpg) (GNUFD)*

Consulteu la secció d'[electrònica](../../electronica) per obtenir més informació sobre aquest circuit.
