---
title: Ventiladors
taxonomy:
    category: docs
process:
        twig: true
---

### Ventiladors

La màquina conté un ordinador d'escriptori i una pantalla que generen calor. L'espai interior és molt gran, però tot i així és important garantir una extracció de l'aire calent que s'hi pugui acumular.

Necessitarem dos ventiladors estàndard de 60mm de diàmetre, com els que s'utilitzen per a les caixes d'ordinador, així com dues reixetes protectores per evitar que s'hi puguin introduir objectes o algú hi pugui posar els dits sense voler. Els ventiladors s'instal·len a la part superior de la màquina perquè l'aire calent és menys dens i té tendència a pujar, així com per fer-los menys visibles i accessibles.

![](ventiladors.jpg)
*Foto: [Howard Community College - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Wikiversity_model_boat_project_-_two_pc_fans.jpg) (CC BY-SA 3.0)*
