---
title: Impressora 3D
taxonomy:
    category: docs
process:
        twig: true
---

### Impressora 3D

Qualsevol impressora 3D us servirà. En cas de no tenir-ne una, podeu contactar amb algun FabLab o bé fer ús d'algun servei de fabricació en línia, com ara [3D Hubs](https://www.3dhubs.com/).

En qualsevol cas, la impressora només és necessària per fabricar el [suport de la roseta de xarxa i el connector de corrent](../suport). Si ho preferiu, podeu fabricar-vos una peça a mà en algun altre material, com ara fusta o metacrilat.
