---
title: Trepant
taxonomy:
    category: docs
process:
        twig: true
---

### Trepant

Necessitareu un trepant elèctric per pre-perforar tots els forats on hi hàgiu de posar cargols. És molt important seguir aquestes instruccions perquè, tot i que el contraplacat és un material dur i resistent, pot esquerdar-se si hi intentem fer entrar cargols directament, especialment a les zones properes a cantonades i vores.

![](trepant.jpg)
*[George Shuklin - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Soviet_drill.jpg) (CC0)*
