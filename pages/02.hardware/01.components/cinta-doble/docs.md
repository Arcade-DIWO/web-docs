---
title: Cinta de doble cara
taxonomy:
    category: docs
process:
        twig: true
---

### Cinta de doble cara

Per enganxar el lladre de corrent a la carcassa utilitzarem cinta de doble cara. Si el vostre lladre té forats de muntatge, podeu utilitzar cargols.
