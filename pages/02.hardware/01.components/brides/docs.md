---
title: Brides
taxonomy:
    category: docs
process:
        twig: true
---

### Brides

Per agrupar cables (de darrera la botonera, per exemple) podeu utilitzar brides, plàstic termofusible o fins i tot els filferros de les bosses de pa de motlle.
