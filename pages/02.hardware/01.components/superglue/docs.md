---
title: Super Glue
taxonomy:
    category: docs
process:
        twig: true
---

### Super Glue

Mai està de més comptar amb un adhesiu potent. En el nostre cas, ens ha calgut per reforçar l'adhesió de la [cinta de les portes](../cinta-juntes).
