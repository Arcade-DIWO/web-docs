---
title: Alicates
taxonomy:
    category: docs
process:
        twig: true
---

### Alicates

Necessitareu unes alicates de tall per tallar cables.

![](alicates.jpg)
*Foto: [Pixabay](https://pixabay.com/en/pliers-cutter-equipment-tool-855717/) (CC0)*
