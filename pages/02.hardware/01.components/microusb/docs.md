---
title: Cable micro USB
taxonomy:
    category: docs
process:
        twig: true
---

### Cable micro USB

La [placa Arduino](../arduino) no inclou el cable micro USB que necessitareu tant per a programar-la com per a connectar-la definitivament a l'ordinador intern de la màquina.

![](cable.jpg)
*Foto: [Masamic](https://commons.wikimedia.org/wiki/User:Masamic) - [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:MicroB_USB_Plug.jpg) (CC BY-SA-3.0)*

Us caldrà una llargada aproximada de 150cm. Compteu que el cable ha d'anar des de la placa, que va adherida a la part de darrera de la botonera, fins a un port USB de l'ordinador, que queda situat al terra de la carcassa.
