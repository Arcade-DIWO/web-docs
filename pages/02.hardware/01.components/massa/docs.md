---
title: Massa de fusta
taxonomy:
    category: docs
process:
        twig: true
---

### Massa de fusta

Algunes de les mides de la carcassa estan pensades perquè encaixin amb molt poca tolerància, i us caldrà una massa de fusta o goma (mai de metall, marcaríeu la fusta!) per fer-les entrar. Si no teniu una massa, us poden servir uns serjants, però en alguns casos hauran de ser molt llargs.

![](massa.jpg)

*Foto: pròpia*
