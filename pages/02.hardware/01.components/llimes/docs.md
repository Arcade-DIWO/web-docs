---
title: Llimes i paper de vidre
taxonomy:
    category: docs
process:
        twig: true
---

### Llimes i paper de vidre

La [fresadora](../cnc) deixarà les vores força rasposes i caldrà que hi feu algunes passades amb paper de vidre o llima per deixar-les polides.
