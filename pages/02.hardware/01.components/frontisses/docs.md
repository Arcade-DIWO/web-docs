---
title: Frontisses de piano
taxonomy:
    category: docs
process:
        twig: true
---

### Frontisses de piano

Tant la porta de davant com la de darrera van unides a la carcassa mitjançant frontisses de piano. Us en caldran entre 53 i 40cm per a la de davant, i entre 100cm i 115cm per a la de darrera.
