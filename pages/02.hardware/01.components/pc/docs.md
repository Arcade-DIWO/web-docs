---
title: Ordinador de sobretaula
taxonomy:
    category: docs
process:
	twig: true
---

### Ordinador de Sobretaula

La màquina porta un ordinador de sobretaula amb un [Debian Linux](https://debian.org) personalitzat que executa un software a mida en arrencar.

Ens servirà qualsevol ordinador de sobretaula, sempre que tingui:

* Tarja de so
* Tarja de xarxa Ethernet
* Tarja gràfica (amb acceleració 3D)
* Un port USB lliure
* Possibilitat d'arrencar automàticament en rebre alimentació (via opció de BIOS)

Molt probablement, l'ordinador de sobretaula més barat que pugueu trobar a botiga ja us servirà, sempre i quan pugui arrencar automàticament en rebre alimentació elèctrica. Pregunteu-ho a la mateixa botiga i us sabran assessorar. Sobretot, assegureu-vos que estigueu pagant cap sistema operatiu pre-instal·lat!

Per a la màquina de Pere Quart, nosaltres vam comprar un ordinador amb processador Intel Celeron G3930, 4Gb de RAM i 1TB de disc dur. La tarja gràfica, la tarja de xarxa i la tarja de so van integrades a la placa base i són més que suficients.
