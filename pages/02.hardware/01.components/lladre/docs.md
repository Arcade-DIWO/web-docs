---
title: Lladre
taxonomy:
    category: docs
process:
        twig: true
---

### Lladre

A l'interior de la màquina hi ha uns quants components que necessiten ser connectats a la corrent. Utilitzarem un lladre qualsevol de tres endolls, que modificarem per tal que només calgui un sol cable per endollar la màquina sencera.

![](lladre.jpg)

*Foto: [Santeri Viinamäki - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Schuko_Power_Strip.jpg) (CC-BY-SA 4.0)*

Visiteu l'apartat d'[electrònica](../../electronica) per a més informació sobre el muntatge del lladre.
