---
title: Cola blanca
taxonomy:
    category: docs
process:
        twig: true
---

### Cola blanca

Els suports per a la pantalla estan formats per dues peces que cal unir amb cola blanca. No us en caldrà massa, amb el pot més petit que trobeu a la ferreteria, drogueria o centre de bricolatge en tindreu de sobra.
