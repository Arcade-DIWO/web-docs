---
title: Broques
taxonomy:
    category: docs
process:
        twig: true
---

### Broques

El contraplacat és un material dur i resistent, però que pot esquerdar-se si hi cargolem cargols sense pre-perforar-lo, especialment prop de les vores.

Tant per als cargols estructurals com per a les frontisses, panys, botons, altaveus i altres components a muntar sobre la fusta, us caldrà una broca de 2mm. Si la vostra màquina necessita un control de volum extern, també us caldrà una broca del diàmetre de la tija del potenciòmetre. En el nostre cas vam necessitar una broca de 8mm.
