---
title: Resistències de 220Ω
taxonomy:
    category: docs
process:
        twig: true
---

### Resistències de 220Ω

Cada microinterruptor necessita una resistència de 220Ω. Els joysticks porten quatre microinterruptors, i cada botó en porta un, de manera que ens en faran falta 11 en total. En podeu trobar a qualsevol botiga de components electrònics.

![](resistencia.jpg)
*Foto: [oomlout](https://www.flickr.com/people/33504192@N00) - [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:220_ohms_5%25_axial_resistor.jpg) (CC BY-SA 2.0)*

Podeu consultar els detalls sobre el circuit de la botonera a la secció d'[electrònica](../electronica).
